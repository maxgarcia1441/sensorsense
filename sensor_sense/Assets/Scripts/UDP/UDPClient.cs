using UnityEngine;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System;
using System.Threading;

public class UDPClient
{
    private UdpClient _udpClient;
    private IPEndPoint _endPoint;
    public bool isActive;
    public bool isIPv4 = true;
    private int[] _defaultPorts = { 55000, 56001, 57001 };
    private const string PASSPHRASE = "HelloPotato";
    private bool _isThreadRunning = false;
    private float _searchTime = float.MinValue;
    private float _lastBroadcastTime = float.MinValue;
    private bool matchFound = false;
    public string test = "helloPotato";
    public UDPClient()
    {
    }

    public void InitIPv4Client(IPAddress iPAddress, int endPort)
    {
        isIPv4 = true;
        _udpClient = new UdpClient();
        _endPoint = new IPEndPoint(iPAddress, endPort);
        isActive = true;
    }

    public void InitIPv6Client(IPAddress iPAddress, int endPort)
    {
        isIPv4 = false;
        _udpClient = new UdpClient(AddressFamily.InterNetworkV6);
        _udpClient.Connect(iPAddress, endPort);
        isActive = true;
    }

    public void InitIPv4Broadcast(int endPort)
    {
        isIPv4 = true;
        _udpClient = new UdpClient();
        _endPoint = new IPEndPoint(IPAddress.Broadcast, endPort);
        _udpClient.EnableBroadcast = true;
        isActive = true;
    }

    public void SendMessage(string message)
    {
        if (isActive)
        {
            byte[] messageBytes = Encoding.ASCII.GetBytes(message);
            if (isIPv4)
            {
                _udpClient.Send(messageBytes, messageBytes.Length, _endPoint);
            }
            else
            {
                _udpClient.Send(messageBytes, messageBytes.Length);
            }
        }
        else
        {
            Debug.Log("The client is not active");
        }
    }

    public void StartSearch()
    {
        matchFound = false;
        if (!_isThreadRunning)
        {
            _isThreadRunning = true;
            _searchTime = (float)DateTime.Now.TimeOfDay.TotalSeconds;
            Thread thread = new Thread(BroadcastToAddresses);
            thread.Start();
        }
    }

    private void BroadcastToAddresses()
    {
        int portNumber = 0;
        while (portNumber < _defaultPorts.Length && !matchFound)
        {
            float timeInSeconds = (float)DateTime.Now.TimeOfDay.TotalSeconds;
            if (timeInSeconds - _searchTime > 3.0f)
            {
                InitIPv4Broadcast(_defaultPorts[portNumber]);
                portNumber++;
                _searchTime = (float)DateTime.Now.TimeOfDay.TotalSeconds;
            }
            if (timeInSeconds - _lastBroadcastTime > 0.1f)
            {
                BroadcastMessage(PASSPHRASE);
            }
        }
    }

    private void BroadcastMessage(string message)
    {
        byte[] data = Encoding.ASCII.GetBytes(message);
        _udpClient.Send(data, data.Length, _endPoint);
    }

    public void TestMessage()
    {
        string message = "Hello, UDP Server!";
        byte[] messageBytes = Encoding.ASCII.GetBytes(message);
        if (isIPv4)
        {
            _udpClient.Send(messageBytes, messageBytes.Length, _endPoint);
        }
        else
        {
            _udpClient.Send(messageBytes, messageBytes.Length);
        }
    }

    public void Close()
    {
        _udpClient.Close();
        isActive = false;
    }
}