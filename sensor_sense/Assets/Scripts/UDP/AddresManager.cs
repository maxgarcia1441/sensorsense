using UnityEngine;
using System;
using System.Net;
using System.Net.Sockets;

public class AddressManager
{
    public static string GetLocalIPv4Address()
    {
        String localIPAddress = string.Empty;
        IPAddress localIp;
        try
        {
            // Get the host name
            string hostName = Dns.GetHostName();

            // Get the IP addresses associated with the host
            IPAddress[] addresses = Dns.GetHostAddresses(hostName);

            // Find the first IPv4 address that is not a loopback address
            foreach (IPAddress address in addresses)
            {
                if (address.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork &&
                    !IPAddress.IsLoopback(address))
                {
                    localIPAddress = address.ToString();
                    localIp = address;
                    break;
                }
            }
        }
        catch (Exception e)
        {
            Debug.Log("Failed to retrieve local IP address: " + e.Message);
        }

        return localIPAddress;
    }
    public static bool IsIPv4Address(string input)
    {
        IPAddress ipAddress;
        return IPAddress.TryParse(input, out ipAddress) && ipAddress.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork;
    }

    public static string GetLocalIPv6Address()
    {
        string ipAddress = "";

        try
        {
            // Get the local player's IP address
            string localIPAddress = GetLocalIPv4Address();

            // Get the IPHostEntry for the local IP address
            IPHostEntry hostEntry = Dns.GetHostEntry(localIPAddress);

            // Find the IPv6 address in the IP address list
            foreach (IPAddress address in hostEntry.AddressList)
            {
                if (address.AddressFamily == AddressFamily.InterNetworkV6)
                {
                    ipAddress = address.ToString();
                    break;
                }
            }
        }
        catch (SocketException e)
        {
            Debug.LogError("Error getting local IPv6 address: " + e.Message);
        }

        return ipAddress;
    }

    public static bool IsIPv6Address(string input)
    {
        IPAddress ipAddress;
        return IPAddress.TryParse(input, out ipAddress) && ipAddress.AddressFamily == System.Net.Sockets.AddressFamily.InterNetworkV6;
    }

    public static bool IsPort(string input){
        int udpPort;
        bool isPort = int.TryParse(input, out udpPort) && udpPort >= IPEndPoint.MinPort && udpPort <= IPEndPoint.MaxPort;
        return isPort;
    }

    public static string GetPublicIPv4Address()
    {
        try
        {
            using (var client = new WebClient())
            {
                string ipAddress = client.DownloadString("https://api.ipify.org");
                return ipAddress;
            }
        }
        catch (Exception e)
        {
            Debug.Log("Failed to retrieve public IP address: " + e.Message);
            return string.Empty;
        }
    }
}