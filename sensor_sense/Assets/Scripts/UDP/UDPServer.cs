using UnityEngine;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System;
public class UDPServer
{
    private UdpClient udpServer;
    public string receivedMessage;
    public IPAddress ipAddress;
    public int endPort;
    public int localPort;
    public bool isIPv4 = true;
    public bool isActivated = false;

    private int[] _defaultPorts = { 55000, 56001, 57001 };
    private float _searchTime = float.MinValue;
    public bool isListening = false;
    private bool _isThreadRunning = false;
    private bool matchFound = false;
    private const string PASSPHRASE = "HelloPotato";

    public UDPServer()
    {
    }

    public void SetIPv4Server(int endPort, int localPort)
    {
        isIPv4 = true;
        this.endPort = endPort;
        this.localPort = localPort;
        udpServer = new UdpClient(localPort);
        Debug.Log("Listening now");
        isActivated = true;

        udpServer.BeginReceive(ReceiveCallback, null);
    }

    private void ReceiveCallback(System.IAsyncResult result)
    {
        try
        {
            IPEndPoint endPoint;
            if (isIPv4)
            {
                endPoint = new IPEndPoint(IPAddress.Any, endPort);
            }
            else
            {
                endPoint = new IPEndPoint(IPAddress.IPv6Any, endPort);
            }
            // Receive the message and the client's IP endpoint
            byte[] receivedBytes = udpServer.EndReceive(result, ref endPoint);
            receivedMessage = Encoding.ASCII.GetString(receivedBytes);
            matchFound = receivedMessage.CompareTo(PASSPHRASE) == 0;
            ipAddress = endPoint.Address;
            isListening = true;
            // Restart listening for incoming messages
            udpServer.BeginReceive(ReceiveCallback, null);
        }
        catch (Exception e)
        {
            isActivated = false;
            Debug.LogError(e.Message);
        }
    }

    public void SetIPv6Server(int endPort, int localPort)
    {
        isIPv4 = false;
        this.endPort = endPort;
        this.localPort = localPort;
        udpServer = new UdpClient(new IPEndPoint(IPAddress.IPv6Any, localPort));
        isActivated = true;

        udpServer.BeginReceive(ReceiveCallback, null);
    }

    public void StartSearch()
    {
        matchFound = false;
        if (!_isThreadRunning)
        {
            _isThreadRunning = true;
            _searchTime = (float)DateTime.Now.TimeOfDay.TotalSeconds;
            Thread thread = new Thread(SearchForClientLoop);
            thread.Start();
        }
    }

    private void SearchForClientLoop()
    {
        int portNumber = 0;
        while (!matchFound && isListening && portNumber < _defaultPorts.Length)
        {
            float timeInSeconds = (float)DateTime.Now.TimeOfDay.TotalSeconds;
            if (timeInSeconds - _searchTime > 3.0f)
            {
                SetIPv4Server(endPort, _defaultPorts[portNumber]);
                portNumber++;
                _searchTime = (float)DateTime.Now.TimeOfDay.TotalSeconds;
            }
        }
    }

    public void Close()
    {
        isListening = false;
        isActivated = false;
        udpServer.Close();
    }
}