using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExitGame : MonoBehaviour
{
    public StateData gameState;
    void OnDestroy(){
        gameState.localIPs[0] = null;
        gameState.localIPs[1] = null;
    }
}
