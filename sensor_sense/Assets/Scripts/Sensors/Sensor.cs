using UnityEngine;
using UnityEditor;

public abstract class Sensor : MonoBehaviour
{
    public static float MAX_SAMPLING_RATE { get { return 20.0f; } }

    abstract public bool IsAvailable { get; }
    abstract public float[] SensorValue { get; }
    abstract public string SetActive(bool state);
    abstract public string TryChangeActive();
    abstract public string[,] ValueNames { get; }
    abstract public float[] MeanSensorValue { get; }

    protected float[] _meanValue { get; set; }

    public StateData stateData;

    virtual public bool readyToStream { get; set; }
    virtual public bool isRecording { get; set; }
    virtual public float samplingRate { get; set; } = 2.0f;
    virtual public string ValueString
    {
        get
        {
            float[] valueArray = SensorValue;
            string valueString = "";
            for (int i = 0; i < valueArray.Length; i++)
            {
                valueString += valueArray[i].ToString("0.000000").Replace(",", ".") + ", ";
            }
            return valueString.Substring(0, valueString.Length - 2);
        }
    }
    virtual public void ToggleRecording()
    {
        if (IsAvailable && !isRecording || isRecording)
        {
            isRecording = !isRecording;
        }
        else
        {
            Debug.LogWarning("The sensor has to be activated to save data");
            DisplayMessage("The sensor has to be activated to save data");
        }
    }

    virtual public void DisplayMessage(string messageString)
    {
        Messages messages = FindObjectOfType<Messages>();
        messages.DisplayMessage(messageString);
    }

    virtual public void DisplayMessage(string messageString, Color messageColor)
    {
        Messages messages = FindObjectOfType<Messages>();
        messages.DisplayMessage(messageString, messageColor);
    }

    protected virtual void Start()
    {
        //stateData = FindScriptableObject<StateData>("stateData");
        isRecording = false;
        readyToStream = false;
        SetActive(true);
    }

    /*private static T FindScriptableObject <T>(string assetName) where T : ScriptableObject
    {
        string type = typeof(T).ToString();
        string[] guids = AssetDatabase.FindAssets("t:" + type + " " + assetName);

        if (guids.Length > 0)
        {
            string assetPath = AssetDatabase.GUIDToAssetPath(guids[0]);
            return AssetDatabase.LoadAssetAtPath<T>(assetPath);
        }

        return null;
    }*/

}