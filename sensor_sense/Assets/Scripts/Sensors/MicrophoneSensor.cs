using UnityEngine;
using UnityEngine.UI;
using System;

public class MicrophoneSensor : Sensor
{
    public AudioSource source;
    private AudioClip microphoneClip;
    public string microphoneName;
    bool used = false;

    public override bool IsAvailable
    {
        get
        {
            return microphoneName != null && microphoneName != "";
        }
    }

    public override float[] SensorValue
    {
        get
        {
            float[] valueArray = new float[1];
            int startPosition = Microphone.GetPosition(Microphone.devices[0]) - 1024;
            if (startPosition < 0)
            {
                valueArray[0] = 0.0f;
                return valueArray;
            }
            float[] waveData = new float[1024];
            microphoneClip.GetData(waveData, startPosition);

            float totalLoudness = 0;
            for (int i = 0; i < 1024; i++)
            {
                totalLoudness += Mathf.Abs(waveData[i]);
            }

            float averageLoudness = totalLoudness / 1024;

            // Convert averageLoudness to decibels using the calibration factor
            float intensityInDecibels = 20 * Mathf.Log10(averageLoudness * 200000f);
            valueArray[0] = intensityInDecibels;
            return valueArray;
        }
    }

    public override string SetActive(bool state)
    {
        try
        {
            if (state)
            {
                microphoneName = Microphone.devices[0];
                microphoneClip = Microphone.Start(microphoneName, true, 20, AudioSettings.outputSampleRate);
                if (IsAvailable != state)
                {
                    return "There is no microphone available";
                }
                stateData.microphoneActivated = IsAvailable;
            }
            else
            {
                Microphone.End(microphoneName);
                stateData.microphoneActivated = IsAvailable;
            }
            return null;
        }
        catch (Exception e)
        {
            return e.Message;
        }
    }

    public override string TryChangeActive()
    {
        return SetActive(!stateData.microphoneActivated);
    }
    public override string[,] ValueNames
    {
        get
        {
            string[,] names = new string[1, 2];
            names[0, 0] = "Sound";
            names[0, 1] = "dB";
            return names;
        }
    }
    public override float[] MeanSensorValue
    {
        get
        {
            return _meanValue;
        }
    }

    ///////////////// Start and Update

    protected override void Start()
    {
        base.Start();
        _meanValue = new float[3];
    }

    void Update()
    {
        float loudnessInDB = SensorValue[0];

        if (Time.time > 5.0f && !used)
        {
            //Microphone.End(microphoneName);
            used = true;
            //Microphone.End(null);
            source.clip = microphoneClip;
            //source.Play();
            Debug.Log("Now playing");
        }

        if (IsAvailable)
        {
            for (int i = 0; i < SensorValue.Length; i++)
            {
                _meanValue[i] = (_meanValue[i] * (Time.time - Time.deltaTime) + SensorValue[i] * Time.deltaTime) / Time.time;
            }
        }
    }

    ///////////////// Specific to the Sensor

}