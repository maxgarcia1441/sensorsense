using UnityEngine;
using System;

public class Gyroscope : Sensor
{
    private Quaternion offset = Quaternion.identity;
    public GameObject testObject;
    public override bool IsAvailable { get { return Input.gyro.enabled; } }
    public override float samplingRate{get; set;} = 7.0f;

    public override float[] SensorValue
    {
        get
        {
            Quaternion rot = Input.gyro.attitude;
            Quaternion rot2 = new Quaternion(rot.x, rot.z, rot.y, -rot.w);
            float[] rotationArray = { rot2.eulerAngles.x, rot2.eulerAngles.y, rot2.eulerAngles.z };
            return rotationArray;
        }
    }

    public override string SetActive(bool state)
    {
        try
        {
            Input.gyro.enabled = state;
            stateData.gyroActivated = state;
        }
        catch (Exception e)
        {
            Debug.LogError("Unable to access Gyroscope " + e.Message);
            return "Unable to access Gyroscope ";
        }
        return null;
    }

    public override string TryChangeActive()
    {
        return SetActive(!stateData.gyroActivated);
    }

    public override string[,] ValueNames
    {
        get
        {
            string[,] names = new string[3, 2];
            names[0, 0] = "X";
            names[0, 1] = "°";
            names[1, 0] = "Y";
            names[1, 1] = "°";
            names[2, 0] = "Z";
            names[2, 1] = "°";
            return names;
        }
    }

    public override float[] MeanSensorValue
    {
        get
        {
            return _meanValue;
        }
    }

    ///////////////// Start and Update

    protected override void Start()
    {
        base.Start();
        _meanValue = new float[3];
    }

    void Update()
    {
        if (stateData.gyroActivated)
        {
            for (int i = 0; i < SensorValue.Length; i++)
            {
                _meanValue[i] = (_meanValue[i] * (Time.time - Time.deltaTime) + SensorValue[i] * Time.deltaTime) / Time.time;
            }
            /*currentRotation = ValueNativeFormat;
            if (testObject != null)
            {
                newDelta = (currentRotation * Quaternion.Inverse(oldRotation)).eulerAngles.magnitude;
                //Debug.Log(oldDelta / newDelta);
                if ((newDelta / oldDelta) > 14)
                {
                    //Debug.Log("A PEAAAAAAAAAAAAAAAK");
                }
                else
                {
                    if (testObject != null && testObject.activeSelf)
                    {
                        testObject.transform.rotation *= currentRotation * Quaternion.Inverse(oldRotation);
                    }
                }
                oldDelta = newDelta;
                oldRotation = ValueNativeFormat;
            }*/
        }
    }

    ///////////////// Specific to the Sensor
    public Quaternion ValueNativeFormat
    {
        get
        {
            Quaternion rot = Input.gyro.attitude;
            Quaternion rot2 = new Quaternion(rot.x, rot.z, rot.y, -rot.w);
            return rot2;
        }
    }

    public void ResetOrientation()
    {
        offset = testObject.transform.rotation;
        stateData.offsetRotation = offset;
        testObject.transform.rotation = Quaternion.identity;
    }
}
