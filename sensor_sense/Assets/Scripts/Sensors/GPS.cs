using UnityEngine;
using System.IO;
using UnityEngine.Android;
using System;

public class GPS : Sensor
{
    private LocationService locationService;

    public override bool IsAvailable
    {
        get
        {
            try
            {
                bool status = locationService.status == LocationServiceStatus.Initializing || locationService.status == LocationServiceStatus.Running;
                bool permission = Permission.HasUserAuthorizedPermission(Permission.FineLocation) && locationService.isEnabledByUser;
                return status && permission;
            }
            catch (Exception e)
            {
                //Debug.LogWarning(e.Message);
                return false;
            }
        }
    }

    public override float[] SensorValue
    {
        get
        {
            if (IsAvailable)
            {
                // Get the current GPS coordinates
                float[] coordinates = { locationService.lastData.latitude, locationService.lastData.longitude, locationService.lastData.altitude };
                return coordinates;
            }
            else
            {
                float[] error = { -1, -1, -1 };
                return error;
            }
        }
    }

    public override string SetActive(bool state)
    {
        if (state)
        {
            locationService = Input.location;
            if (!(Permission.HasUserAuthorizedPermission(Permission.FineLocation) && locationService.isEnabledByUser))
            {
                Permission.RequestUserPermission(Permission.FineLocation);
            }
            if (!(locationService.status == LocationServiceStatus.Initializing || locationService.status == LocationServiceStatus.Running))
            {
                locationService.Start();
            }

            if (!IsAvailable)
            {
                Debug.LogWarning("No location permission given");
                return "No location permission given";
            }
            stateData.locationActivated = IsAvailable;
        }
        else
        {
            locationService.Stop();
            stateData.locationActivated = IsAvailable;
        }
        return null;
    }

    public override string TryChangeActive()
    {
        return SetActive(!stateData.locationActivated);
    }

    public override string[,] ValueNames
    {
        get
        {
            string[,] names = new string[3, 2];
            names[0, 0] = "Latitude";
            names[0, 1] = "°";
            names[1, 0] = "Longitude";
            names[1, 1] = "°";
            names[2, 0] = "Altitude";
            names[2, 1] = "m";
            return names;
        }
    }

    public override float[] MeanSensorValue
    {
        get
        {
            return _meanValue;
        }
    }

///////////////// Start and Update

    protected override void Start()
    {
        base.Start();
        _meanValue = new float[3];
        locationService = Input.location;
    }
    void Update()
    {
        for (int i = 0; i < SensorValue.Length; i++)
        {
            _meanValue[i] = (_meanValue[i] * (Time.time - Time.deltaTime) + SensorValue[i] * Time.deltaTime) / Time.time;
        }
    }

///////////////// Specific to the Sensor

    void OnDestroy()
    {
        if (locationService != null)
        {
            locationService.Stop();
        }
    }

    public Vector3 ValueNativeFormat
    {
        get
        {
            float[] cArray = SensorValue;
            return new Vector3(cArray[0], cArray[1], cArray[2]);
        }
    }
}
