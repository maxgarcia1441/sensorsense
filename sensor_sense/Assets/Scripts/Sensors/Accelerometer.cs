using UnityEngine;
using System;

public class Accelerometer : Sensor
{
    public override bool IsAvailable { get { return Input.gyro.enabled; } }

    public override float samplingRate{get; set;} = 7.0f;

    public override float[] SensorValue
    {
        get
        {
            Vector3 rot = Input.acceleration;
            float[] rotationArray = { rot.x, rot.y, rot.z };
            return rotationArray;
        }
    }

    public override string SetActive(bool state)
    {
        try
        {
            Input.gyro.enabled = state;
            stateData.accelerometerActivated = state;
        }
        catch (Exception e)
        {
            Debug.LogError("Unable to access Accelerometer " + e.Message);
            return "Unable to access Accelerometer ";
        }
        return null;
    }

    public override string TryChangeActive()
    {
        return SetActive(!stateData.accelerometerActivated);
    }

    public override string[,] ValueNames
    {
        get
        {
            string[,] names = new string[3, 2];
            names[0, 0] = "X";
            names[0, 1] = "g";
            names[1, 0] = "Y";
            names[1, 1] = "g";
            names[2, 0] = "Z";
            names[2, 1] = "g";
            return names;
        }
    }

    public override float[] MeanSensorValue
    {
        get
        {
            return _meanValue;
        }
    }

    ///////////////// Start and Update

    protected override void Start()
    {
        base.Start();
        _meanValue = new float[3];
    }

    void Update()
    {
        if (stateData.accelerometerActivated)
        {
            for (int i = 0; i < SensorValue.Length; i++)
            {
                _meanValue[i] = (_meanValue[i] * (Time.time - Time.deltaTime) + SensorValue[i] * Time.deltaTime) / Time.time;
            }
            /*currentRotation = ValueNativeFormat;
            if (testObject != null)
            {
                newDelta = (currentRotation * Quaternion.Inverse(oldRotation)).eulerAngles.magnitude;
                //Debug.Log(oldDelta / newDelta);
                if ((newDelta / oldDelta) > 14)
                {
                    //Debug.Log("A PEAAAAAAAAAAAAAAAK");
                }
                else
                {
                    if (testObject != null && testObject.activeSelf)
                    {
                        testObject.transform.rotation *= currentRotation * Quaternion.Inverse(oldRotation);
                    }
                }
                oldDelta = newDelta;
                oldRotation = ValueNativeFormat;
            }*/
        }
    }

    ///////////////// Specific to the Sensor
    public Vector3 ValueNativeFormat
    {
        get
        {
            return Input.acceleration;
        }
    }
}
