﻿/* 
 * Modified 11.07.2023
 * Author: Etienne Frank, Max Garcia Hinojosa
*/

using UnityEngine;
using System;

public class LightSensor : Sensor
{
    private AndroidJavaObject activityContext;
    private AndroidJavaObject jo;
    AndroidJavaClass activityClass;
    public float maxLux = float.MinValue;
    public override bool IsAvailable { get { return (activityContext != null && jo != null); } }

    public override float[] SensorValue
    {
        get
        {
            float[] value = { -1.0f };
#if UNITY_ANDROID && !UNITY_EDITOR
            value[0] = jo.Call<float>("getLux");
            return value;
#else
            return value;
#endif
        }
    }

    public override string SetActive(bool state)
    {
        string errorMessage = null;
        if (state && !IsAvailable)
        {
            try
            {
#if UNITY_ANDROID
                activityClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
                activityContext = activityClass.GetStatic<AndroidJavaObject>("currentActivity");
                jo = new AndroidJavaObject("com.etiennefrank.lightsensorlib.LightSensorLib");
                stateData.lightActivated = IsAvailable;
                jo.Call("init", activityContext);
                return errorMessage;
#else
                Debug.LogWarning("Light sensor Requires Android");
                errorMessage = "Light sensor Requires Android";
#endif
            }
            catch (Exception e)
            {
                Debug.LogWarning("Activation failed" + e.Message);
                errorMessage = "Activation failed";
            }
            stateData.lightActivated = IsAvailable;
            return errorMessage;
        }
        else
        {
            activityContext = null;
            jo = null;
            stateData.lightActivated = IsAvailable;
            return errorMessage;
        }
    }

    public override string TryChangeActive()
    {
        return SetActive(!stateData.lightActivated);
    }

    public override string[,] ValueNames
    {
        get
        {
            string[,] names = new string[1, 2];
            names[0, 0] = "Intesity";
            names[0, 1] = "Lux";
            return names;
        }
    }

    public override float[] MeanSensorValue
    {
        get
        {
            return _meanValue;
        }
    }

    ///////////////// Start and Update

    protected override void Start()
    {
        base.Start();
        _meanValue = new float[1];
    }

    void Update()
    {
        if (IsAvailable)
        {
            for (int i = 0; i < SensorValue.Length; i++)
            {
                _meanValue[i] = (_meanValue[i] * (Time.time - Time.deltaTime) + SensorValue[i] * Time.deltaTime) / Time.time;
            }
            if (SensorValue[0] > maxLux)
            {
                maxLux = SensorValue[0];
            }
        }
    }

    ///////////////// Specific to the Sensor

    public float ValueNativeFormat
    {
        get
        {
#if UNITY_ANDROID
            return jo.Call<float>("getLux");
#else
            return -1.0f;
#endif
        }
    }

}
