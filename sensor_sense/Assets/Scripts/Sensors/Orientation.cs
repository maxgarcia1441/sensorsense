using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Orientation : MonoBehaviour
{
    public bool landscapeLeft;
    public bool landscapeRight;
    public bool portrait;
    public bool portraitUpsideDown;
    
    void Start()
    {
        Screen.autorotateToPortrait = portrait;
        Screen.autorotateToPortraitUpsideDown = portraitUpsideDown;
        Screen.autorotateToLandscapeLeft = landscapeLeft;
        Screen.autorotateToLandscapeRight = landscapeRight;
    }
    void Update()
    {
        
    }

    private void SetOrientation(ScreenOrientation orientation)
    {
        //Kind of useless now...
        Screen.autorotateToPortrait = orientation == ScreenOrientation.AutoRotation;
        Screen.autorotateToPortraitUpsideDown = orientation == ScreenOrientation.AutoRotation;
        Screen.autorotateToLandscapeLeft = orientation == ScreenOrientation.AutoRotation;
        Screen.autorotateToLandscapeRight = orientation == ScreenOrientation.AutoRotation;
        Screen.orientation = orientation;
    }
}
