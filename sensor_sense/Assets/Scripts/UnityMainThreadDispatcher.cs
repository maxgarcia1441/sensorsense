using System.Collections.Generic;
using UnityEngine;

public class UnityMainThreadDispatcher : MonoBehaviour
{
    private static readonly Queue<System.Action> queue = new Queue<System.Action>();

    private void Update()
    {
        lock (queue)
        {
            while (queue.Count > 0)
            {
                queue.Dequeue().Invoke();
            }
        }
    }
// you can only call this thing once
    public static void Enqueue(System.Action action)
    {
        lock (queue)
        {
            queue.Enqueue(action);
        }
    }
}