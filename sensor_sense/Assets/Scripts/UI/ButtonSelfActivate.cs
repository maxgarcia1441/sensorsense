using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;

public class ButtonSelfActivate : MonoBehaviour
{
    public Sensor sensor;
    public Sprite enabledColor;
    public Sprite disabledColor;
    public void OnClick()
    {
        sensor.readyToStream = !sensor.readyToStream;
        if (sensor.readyToStream)
        {
            GetComponent<Button>().image.sprite = enabledColor;
            GetComponent<Button>().image.color = StreamingScreen.greenEnabledColor;
        }
        else
        {
            GetComponent<Button>().image.sprite = disabledColor;
            GetComponent<Button>().image.color = StreamingScreen.enabledColor;
        }
    }

    public void AddComponents(Sensor sensor, Sprite enabledColor, Sprite disabledColor){
        this.sensor = sensor;
        this.enabledColor = enabledColor;
        this.disabledColor = disabledColor;
    }
}
