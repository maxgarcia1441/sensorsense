using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Messages : MonoBehaviour
{
    public float duration;   // [duration, time counter]
    private bool errorON;
    public static Color errorColor= new Color(233f/ 255, 79f/ 255, 55f/ 255, 255f/ 255);
    public static Color warningColor = new Color(245f/ 255, 187f/ 255, 0f/ 255, 255f/ 255);
    public static Color successColor = new Color(255f/ 255, 60f/ 255, 199f/ 255, 255f/ 255);
    public AnimationCurve fadingEffect;
    private Queue<Message> errorMessages = new Queue<Message>();
    private Message currentError;
    public TextMeshProUGUI errorText;
    private class Message
    {
        public string errorMessage;
        public Color color;
        public float initTime;
        public Message(string errorMessage, Color errorColor)
        {
            this.errorMessage = errorMessage;
            this.color = errorColor;
        }
    }
    void Start()
    {

    }

    void Update()
    {
        if (errorMessages.Count > 0 && !errorON)
        {
            errorON = true;
            currentError = errorMessages.Dequeue();
            errorText.text = currentError.errorMessage;
            errorText.gameObject.SetActive(true);
            currentError.initTime = Time.time;
        }
        if (errorON)
        {
            if (Time.time - currentError.initTime >= duration)
            {
                errorText.gameObject.SetActive(false);
                errorON = false;
            }
            Color color = new Color(currentError.color.r, currentError.color.g, currentError.color.b, fadingEffect.Evaluate((Time.time - currentError.initTime) / duration));
            errorText.color = color;
        }
    }

    public void DisplayMessage(string errorString)
    {
        if (errorString != null)
        {
            if (errorMessages.Count < 4)
            {
                Message errorMessage = new Message(errorString, errorColor);
                errorMessages.Enqueue(errorMessage);
            }
        }
    }

    public void DisplayMessage(string errorString, Color errorColor)
    {
        if (errorString != null)
        {
            if (errorMessages.Count < 4)
            {
                Message errorMessage = new Message(errorString, errorColor);
                errorMessages.Enqueue(errorMessage);
            }
        }
    }
}
