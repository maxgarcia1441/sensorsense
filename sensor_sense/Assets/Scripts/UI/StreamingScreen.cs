using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Net;
using UnityEngine.UI;

public class StreamingScreen : MonoBehaviour
{
    public StateData stateData;
    public UDPClient client = new UDPClient();
    public UDPServer server = new UDPServer();
    private Messages _messages;
    public Text inputServerPort;
    public Text inputClientPort;
    public Text inputIP;
    private Sensor[] _sensors;
    private ButtonSensorPair[] buttonSensorPairs;
    public Button dummyButton;
    public GameObject buttonContainer;
    public Sprite enabledSprite;
    public Sprite greenEnabledSprite;
    public Sprite greenCircle;
    public Sprite redCircle;
    public static Color enabledColor = new Color(1.0f, 0.908f, 0.956f);
    public static Color greenEnabledColor = new Color(1.0f, 0.776f, 0.906f);
    public static Color redDisabledColor = new Color(0.988f, 0.992f, 0.8f);
    public Button clientActivateButton;
    public Button toggleUseTimeButton;
    public Slider frequencySlider;
    public Text frequencySlideText;
    private StreamData _streamData;
    void Start()
    {
        _messages = FindObjectOfType<Messages>();
        stateData = FindObjectOfType<StateData>();
        _sensors = FindObjectsOfType<Sensor>();
        _streamData = FindObjectOfType<StreamData>();
        buttonSensorPairs = new ButtonSensorPair[_sensors.Length];
        for (int i = 0; i < _sensors.Length; i++)
        {
            if (i == 0)
            {
                buttonSensorPairs[i] = new ButtonSensorPair(dummyButton, _sensors[i]);
            }
            else
            {
                buttonSensorPairs[i] = new ButtonSensorPair(Instantiate(dummyButton), _sensors[i]);
            }
            buttonSensorPairs[i].button.transform.SetParent(buttonContainer.transform);
            buttonSensorPairs[i].button.name = "button" + (i);
            buttonSensorPairs[i].button.GetComponentInChildren<Text>().text = _sensors[i].GetType().ToString();
            buttonSensorPairs[i].button.GetComponent<ButtonSelfActivate>().AddComponents(buttonSensorPairs[i].sensor, greenEnabledSprite, enabledSprite);
        }
    }
    void Update()
    {
        for (int i = 0; i < _sensors.Length; i++)
        {
            buttonSensorPairs[i].button.interactable = buttonSensorPairs[i].sensor.IsAvailable;
        }
        if (client.isActive)
        {
            clientActivateButton.image.sprite = greenCircle;
            clientActivateButton.image.color = greenEnabledColor;
        }
        else
        {
            clientActivateButton.image.sprite = redCircle;
            clientActivateButton.image.color = redDisabledColor;
        }
        _streamData.streamingFrequency = frequencySlider.value;
        frequencySlideText.text = (_streamData.streamingFrequency * StreamData.MAX_STREAMING_FREQUENCY).ToString("0.0");
    }

    public void ToggleUseTime()
    {
        _streamData.appendTime = !_streamData.appendTime;
        if (_streamData.appendTime)
        {
            toggleUseTimeButton.image.sprite = greenCircle;
            toggleUseTimeButton.image.color = greenEnabledColor;
        }
        else
        {
            toggleUseTimeButton.image.sprite = redCircle;
            toggleUseTimeButton.image.color = redDisabledColor;
        }
    }

    public void ToggleUDPClientActive()
    {
        SetUDPClientActive(!client.isActive);
    }

    public void SetUDPClientActive(bool state)
    {
        int port = -1;
        IPAddress address = null;
        if (state)
        {
            address = ChecktIP(inputIP.text);
            port = CheckPort(inputClientPort.text);
            if (state && address != null && port != -1)
            {
                client.InitIPv4Client(address, port);
                _messages.DisplayMessage("Client now bound to:\n" + address.ToString() + "\nport: " + port, Messages.successColor);
            }
        }
        else
        {
            client.Close();
        }
    }

    //Make a menu that shows only the active sensors to select
    //what data to stream

    public int CheckPort(string inputPort)
    {
        if (inputPort == "Enter here" || inputPort == "")
        {
            _messages.DisplayMessage("Specify a port first!");
            return -1;
        }
        else if (AddressManager.IsPort(inputPort))
        {
            return int.Parse(inputPort);
        }
        else
        {
            _messages.DisplayMessage("That's not a valid port!");
            return -1;
        }
    }

    public IPAddress ChecktIP(string inputIP)
    {
        if (inputIP == "Enter here" || inputIP == "")
        {
            _messages.DisplayMessage("Using local IP address", Messages.successColor);
            return IPAddress.Parse(AddressManager.GetLocalIPv4Address());
        }
        else if (AddressManager.IsIPv4Address(inputIP))
        {
            return IPAddress.Parse(inputIP);
        }
        else
        {
            _messages.DisplayMessage("That's not a valid IP address!");
            return null;
        }
    }

    class ButtonSensorPair
    {
        public Button button;
        public Sensor sensor;
        public ButtonSensorPair(Button button, Sensor sensor)
        {
            this.button = button;
            this.sensor = sensor;
        }
    }

}
