﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/**
 * Draws a graph of lines by means of OnGUI 
 */
public class LineGenerator : MonoBehaviour
{
    [SerializeField]
    public Texture2D texture;
    public Color valueGraphColor;
    [Tooltip("High value is considered to be any value grater 60fps")]
    public Color valueHighLineIndicatorColor;
    [Tooltip("Normal value is considered to be 60fps")]
    public Color valueNormalLineIndicatorColor;

    private const float TOP_NORMAL_MARGIN_SCALE = 0.45f;
    private const float TOP_MAX_MARGIN_SCALE = 0.22f;
    private const int MIN_SIZE = 100;

    private Text _valueText;
    private string _valueString = "";
    private float _fullHeight = 0f;
    private float _oldFullHeight = 0f;
    private float _maxHeight = 0f;
    private float _oldMaxHeight = 0f;
    private bool _canStart = false;
    private Queue<float> _heightsQueue;
    private Vector2 _bottomLeft = new Vector2(0f, 0f);
    private RectTransform _rect;
    private float _lastUpdateTime = 0;
    private float _initWidth;
    private bool queueInit;

    public int dataPoints = 100;
    public float valueNormal = 600f;
    public float minValue = 0.0f;
    public bool isUpdating = true;
    public bool demoMode = true;
    public float value;
    public string valueUnits;
    public float updatePeriodMs;
    public FPS fps;
    public void Start()
    {
        StartCoroutine("InitializationRoutine");
        _valueText = transform.GetChild(0).GetChild(0).GetComponent<Text>();

        Vector3 childCoordinates = transform.GetChild(0).position;
        Rect imageRect = transform.GetChild(0).GetComponent<Image>().rectTransform.rect;
        _initWidth = transform.GetComponent<RectTransform>().rect.width;
    }

    public IEnumerator InitializationRoutine()
    {
        // Debug.Log("Routine started, we are going to skeep a frame and then come back");
        yield return null;

        // Debug.Log("One frame is skipped.");

        _rect = transform.GetChild(0).GetComponent<RectTransform>();

        _bottomLeft = _rect.offsetMin;

        if (_rect.rect.width < MIN_SIZE || _rect.rect.width < MIN_SIZE)
        {
            throw new System.Exception("You need to specify Width and Height in Editor for the LineRenderer");
        }

        //dataPoints = (int)_rect.rect.width;
        _fullHeight = _rect.rect.height - (_rect.rect.height * TOP_NORMAL_MARGIN_SCALE);
        _maxHeight = _rect.rect.height - (_rect.rect.height * TOP_MAX_MARGIN_SCALE);
        if (!queueInit)
        {
            InitHeightsQueue();
        }
        _canStart = true;
    }

    /**
     * Creates a queue of zeroes with a size of LineRenderer image width
     */
    private void InitHeightsQueue()
    {
        queueInit = true;
        _heightsQueue = new Queue<float>();

        for (int i = 0; i < dataPoints; i++)
        {
            _heightsQueue.Enqueue(0f);
        }
    }

    public void Update()
    {
        if (_canStart && isUpdating && (Time.time - _lastUpdateTime) >= updatePeriodMs / 1000)
        {
            _lastUpdateTime = Time.time;
            if (_fullHeight != _oldFullHeight)
            {
                _fullHeight = _rect.rect.height - (_rect.rect.height * TOP_NORMAL_MARGIN_SCALE);
            }
            if (_maxHeight != _oldMaxHeight)
            {
                _maxHeight = _rect.rect.height - (_rect.rect.height * TOP_MAX_MARGIN_SCALE);
            }
            if (demoMode)
            {
                value = fps.value;
                _valueString = string.Format("{0:0.0} ms ({1:0.0} fps)", fps.msec, value);
            }
            else
            {
                _valueString = string.Format("{0:0.00} s, {1:0.0} " + valueUnits, Time.time, value);
            }
            _valueText.text = _valueString;

            //Remove first line in order and add a new line to the end

            _heightsQueue.Dequeue();

            float lineHeight = _fullHeight * (value - minValue) / (Mathf.Abs(minValue) + Mathf.Abs(valueNormal));  // where fps / FPS_NORMAL is a coefficient of current fps value to normal 60 fps

            if (lineHeight > _maxHeight)
            {
                lineHeight = _maxHeight;
            }
            else if (lineHeight < 0.0f)
            {
                lineHeight = 0.0f;
            }

            _heightsQueue.Enqueue(lineHeight);
        }
    }

    /**
     * Draws texture with a size 2x2 px in a rectangle with a size 1xlineHeight px 
     */
    public void OnGUI()
    {
        if (_canStart)
        {
            RectTransform containerRect = transform.GetComponent<RectTransform>();
            float counter = 0;
            foreach (float lineHeight in _heightsQueue)
            {
                // Position of GUI layer is top left corner of the screen
                float posX = Mathf.RoundToInt(counter * (containerRect.rect.width / dataPoints) * (containerRect.rect.width / _initWidth))
                    + transform.position.x - containerRect.rect.width / 2 - 1;
                float posY = Screen.height - containerRect.position.y + containerRect.rect.height / 2;
                GUI.color = valueGraphColor;
                GUI.DrawTexture(new Rect(new Vector2(posX, posY - lineHeight), new Vector2(3, 3)), texture);
                counter++;
            }
        }
        DrawNormalFpsLineIndicator();
    }

    /**
     * Draws a normal fps line indicator with width = rectangle width, height=1px
     * and a high fps line indicator above it.
     * Both lines must be drawn above the fps graph rectangular area.
    */
    private void DrawNormalFpsLineIndicator()
    {
        _fullHeight = _rect.rect.height - (_rect.rect.height * TOP_NORMAL_MARGIN_SCALE);
        _maxHeight = _rect.rect.height - (_rect.rect.height * TOP_MAX_MARGIN_SCALE);
        RectTransform containerRect = transform.GetComponent<RectTransform>();
        float posY = Screen.height - containerRect.position.y + containerRect.rect.height / 2;
        GUI.color = valueNormalLineIndicatorColor;
        float factor = (containerRect.rect.width / _initWidth);
        GUI.DrawTexture(new Rect(new Vector2(containerRect.position.x - containerRect.rect.width / 2, posY - _fullHeight), new Vector2((float)(containerRect.rect.width * factor), 1)), texture);
        GUI.color = valueHighLineIndicatorColor;
        GUI.DrawTexture(new Rect(new Vector2(containerRect.position.x - containerRect.rect.width / 2, posY - _maxHeight), new Vector2((float)(containerRect.rect.width * factor), 1)), texture);
    }
}
