using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ValueStringDisplay : MonoBehaviour
{
    private string _valueName = "";
    public float valueFloat;
    public string units;
    private Text valueNameText;
    private Text valueText;
    public string ValueName
    {
        get
        {
            return _valueName;
        }
        set
        {
            _valueName = value;
            valueNameText.text = value;
        }
    }
    void Start()
    {
        valueNameText = transform.GetChild(0).GetComponent<Text>();
        valueText = transform.GetChild(1).GetComponent<Text>();
        valueNameText.text = ValueName;
    }

    void Update()
    {
        valueText.text = valueFloat.ToString("0.000") + " " + units;
    }

}
