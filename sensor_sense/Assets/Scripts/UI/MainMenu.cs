using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class MainMenu : MonoBehaviour
{
    public GameObject mainMenuScreen;
    public GameObject gpsScreen;
    public GameObject lightSensorScreen;
    public GameObject gyroscopeScreen;
    public GameObject microphoneScreen;
    public GameObject accelerometerScreen;
    public TextMeshProUGUI output;
    private GameObject[] _sensorScreens;
    public GameObject settingsScreens;
    public GameObject settingsScreen;

    void Start(){
        _sensorScreens = new GameObject[7];
        _sensorScreens[0] = mainMenuScreen;
        _sensorScreens[1] = gpsScreen;
        _sensorScreens[2] = lightSensorScreen;
        _sensorScreens[3] = gyroscopeScreen;
        _sensorScreens[4] = microphoneScreen;
        _sensorScreens[5] = accelerometerScreen;
        _sensorScreens[6] = settingsScreen;
    }

    public void HandleInput(int val)
    {
        switch (val)
        {
            case 0:
                GoToScreen(lightSensorScreen);
                break;
            case 1:
                GoToScreen(gyroscopeScreen);
                break;
            case 2:
                GoToScreen(gpsScreen);
                break;
            case 3:
                GoToScreen(microphoneScreen);
                break;
            case 4:
                GoToScreen(accelerometerScreen);
                break;
        }
    }

    public void GoToScreen(GameObject screen)
    {
        if (screen == mainMenuScreen || screen == settingsScreen)
        {
            settingsScreens.SetActive(false);
        }
        else
        {
            settingsScreens.SetActive(true);
        }
        bool present = false;
        for (int i = 0; i < _sensorScreens.Length; i++)
        {
            _sensorScreens[i].SetActive(false);
            if (_sensorScreens[i] = screen)
            {
                present = true;
            }
        }
        if (!present)
        {
            GameObject[] temp = _sensorScreens;
            _sensorScreens = new GameObject[temp.Length + 1];
            _sensorScreens[_sensorScreens.Length - 1] = screen;
            Debug.Log("New screen was added");
        }
        screen.SetActive(true);
    }

    public void ExitApp()
    {
        Application.Quit();
    }
}
