using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class SensorScreen : MonoBehaviour
{
    public GameObject screen;
    public Sensor sensor;
    public string value;
    public string units;
    public Button activateButton;
    public Button recordButton;
    public Button saveButton;
    public Button shareButton;
    public AnimationCurve recordingAnimation;
    public float animationDuration;
    public Slider samplingRateSlider;
    public Text samplingRateSliderIndicator;
    public ValueStringDisplay[] valueDisplays;
    public LineGenerator[] graphs;
    private DisplayGraphPair[] _pairs;
    private float[] _valueArray;
    private Dictionary<Button, float> _startTimes = new Dictionary<Button, float>();
    private bool _oldIsRecording;
    public static Color trueColor = new Color(255f / 255f, 198f / 255f, 231f / 255f);
    public static Color falseColor = new Color(255f / 255f, 85f / 255f, 186f / 255f);
    private SaveData _saveDataObject;
    private bool _isUpdatingGraphs = false;
    private float _lastTextUpdate = float.MinValue;
    void Start()
    {
        _saveDataObject = FindObjectOfType<SaveData>();
        _oldIsRecording = sensor.isRecording;
        screen = GetComponent<GameObject>();
        _valueArray = sensor.SensorValue;
        if (valueDisplays.Length != graphs.Length || valueDisplays.Length != _valueArray.Length)
        {
            Debug.LogError("The number of valueDisplays and Graphs must match");
        }
        else
        {
            _pairs = new DisplayGraphPair[valueDisplays.Length];
            for (int i = 0; i < valueDisplays.Length; i++)
            {
                _pairs[i] = new DisplayGraphPair(valueDisplays[i], graphs[i]);
                _pairs[i].graph.demoMode = false;
            }
        }
        if (samplingRateSlider != null)
        {
            samplingRateSlider.value = sensor.samplingRate / Sensor.MAX_SAMPLING_RATE;
        }
    }

    void Update()
    {
        bool containsData = sensor.IsAvailable && _saveDataObject.ContainsData(sensor.GetType());
        SetButton(activateButton, sensor.IsAvailable);
        DisableButton(saveButton, containsData);
        DisableButton(shareButton, containsData);
        float[] values = sensor.SensorValue;
        bool update = Time.time - _lastTextUpdate > 0.15f;
        if (update)
        {
            _lastTextUpdate = Time.time;
        }
        if (values[0] != -1)
        {
            for (int i = 0; i < values.Length; i++)
            {
                if (update)
                {
                    _pairs[i].valueDisplay.valueFloat = values[i];
                    _pairs[i].valueDisplay.ValueName = sensor.ValueNames[i, 0];
                    _pairs[i].valueDisplay.units = sensor.ValueNames[i, 1];
                }
                _pairs[i].graph.value = values[i];
                _pairs[i].graph.valueUnits = sensor.ValueNames[i, 1];
                //_pairs[i].graph.valueNormal = sensor.MeanSensorValue[i];
            }
        }
        else
        {
            for (int i = 0; i < values.Length; i++)
            {
                _pairs[i].valueDisplay.ValueName = "No data";
                _pairs[i].graph.isUpdating = false;
            }
        }
        foreach (Button button in _startTimes.Keys)
        {
            Color cl = button.image.color;
            if (_startTimes[button] != -1.0f)
            {
                float point = (Time.time - _startTimes[button]) / animationDuration;
                point = point - ((int)point);
                cl = new Color(cl.r, cl.g, cl.b, recordingAnimation.Evaluate(point));
                button.image.color = cl;
            }
        }
        if (_oldIsRecording != sensor.isRecording)
        {
            SetRecordAnimation(recordButton, sensor.isRecording);
            _oldIsRecording = sensor.isRecording;
        }

        if (samplingRateSlider != null)
        {
            if (sensor.IsAvailable)
            {
                samplingRateSlider.interactable = true;
                sensor.samplingRate = samplingRateSlider.value * Sensor.MAX_SAMPLING_RATE + 0.00001f;
                samplingRateSliderIndicator.text = (samplingRateSlider.value * Sensor.MAX_SAMPLING_RATE).ToString("0.0");
            }
            else
            {
                samplingRateSlider.interactable = false;
            }
        }
    }

    class DisplayGraphPair
    {
        public ValueStringDisplay valueDisplay;
        public LineGenerator graph;
        public DisplayGraphPair(ValueStringDisplay valueDisplay, LineGenerator graph)
        {
            this.valueDisplay = valueDisplay;
            this.graph = graph;
        }
    }

    public void SetRecordAnimation(Button button, bool state)
    {
        Color cl = button.image.color;
        if (!_startTimes.ContainsKey(button))
        {
            _startTimes.Add(button, Time.time);
        }
        if (state)
        {
            _startTimes[button] = Time.time;
        }
        else
        {
            _startTimes[button] = -1.0f;
            button.image.color = new Color(cl.r, cl.g, cl.b, 1.0f);
        }
    }

    public static void SetButton(Button button, bool state)
    {
        if (state)
        {
            button.image.color = trueColor;
        }
        else
        {
            button.image.color = falseColor;
        }
    }

    public static void DisableButton(Button button, bool state)
    {
        button.interactable = state;
    }

    public void ToggleUpdateGraphs()
    {
        _isUpdatingGraphs = !_isUpdatingGraphs;
        for (int i = 0; i < _pairs.Length; i++)
        {
            _pairs[i].graph.isUpdating = _isUpdatingGraphs;
            Debug.Log(i + " " + _pairs[i].graph.isUpdating);
        }
        if (_isUpdatingGraphs)
        {
            activateButton.GetComponentInChildren<Text>().text = "Stop";
            Debug.Log("text should be " + activateButton.GetComponentInChildren<Text>().text);
        }
        else
        {
            activateButton.GetComponentInChildren<Text>().text = "Resume";
        }
    }
}
