using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisplayAddresses : MonoBehaviour
{
    public StateData stateData;
    public Text localIPText;
    private string localIP;
    void Start()
    {
       /* stateData.localIPs[0] = AddressManager.GetLocalIPv4Address();
        stateData.localIPs[1] = AddressManager.GetLocalIPv6Address();*/
        Debug.Log(stateData.localIPs[1]);
    }
    void Update()
    {
        if (!stateData.UDPServerDisplayData)
        {
            if (stateData.clientServerPair.server.isIPv4)
            {
                localIPText.text = stateData.localIPs[0];
                localIPText.alignment = TextAnchor.UpperRight;
            }
            else
            {
                localIPText.text = stateData.localIPs[1];
                localIPText.alignment = TextAnchor.MiddleLeft;
            }
        }
    }
}
