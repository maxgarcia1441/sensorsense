using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FPS : MonoBehaviour
{
    private float _deltaTime = 0.0f;
    private string _valueString = "";
    private float _lastUpdateTime = 0;
    public bool demoMode = true;
    public float value;
    public string valueUnits;
    public float msec = 0f;
    void Update()
    {
        _lastUpdateTime = Time.time;
        _deltaTime += (Time.deltaTime - _deltaTime) * 0.1f;
        msec = _deltaTime * 1000.0f;
        value = 1.0f / _deltaTime;
    }
}
