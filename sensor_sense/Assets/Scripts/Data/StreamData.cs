using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class StreamData : MonoBehaviour
{
    private Sensor[] _sensors;
    private SensorBoolPair[] _streamingSensors;
    private bool _streamingSensorsChanged = false;
    public static float MAX_STREAMING_FREQUENCY = 20.0f;
    public float streamingFrequency = 2.0f;
    private float lastMessageTime = float.MinValue;
    private UDPClient _udpClient;
    public bool appendTime = true;
    void Start()
    {
        _sensors = FindObjectsOfType<Sensor>();
        _streamingSensors = new SensorBoolPair[_sensors.Length];
        for (int i = 0; i < _sensors.Length; i++)
        {
            _streamingSensors[i] = new SensorBoolPair(_sensors[i]);
        }
        _sensors = null;
        _udpClient = FindObjectOfType<StreamingScreen>().client;
    }

    public string BuildString()
    {
        _streamingSensorsChanged = false;
        string dataToSend = "";
        string header = "";
        if (appendTime)
        {
            dataToSend = DateTime.Now.ToString().Replace(",", ".") + ", ";
            header = "Time, ";
        }
        for (int i = 0; i < _streamingSensors.Length; i++)
        {
            Sensor sensor = _streamingSensors[i].sensor;
            if (_streamingSensors[i].wasStreaming != sensor.readyToStream)
            {
                _streamingSensorsChanged = true;
                if (sensor.readyToStream && sensor.IsAvailable)
                {
                    header += sensor.GetType().ToString() + "/";
                    for (int e = 0; e < sensor.ValueNames.GetLength(0); e++)
                    {
                        header += sensor.ValueNames[e, 0] + ", ";
                    }
                }
            }
            if (sensor.readyToStream && sensor.IsAvailable)
            {
                dataToSend += sensor.ValueString + ", ";
                //There are some commas missing
            }

            _streamingSensors[i].wasStreaming = _streamingSensors[i].sensor.readyToStream;
        }
        string finalString;
        if (_streamingSensorsChanged)
        {
            finalString = header;
        }
        else
        {
            finalString = dataToSend;
        }
        return finalString.Substring(0, finalString.Length - 2);
    }

    void Update()
    {
        if (_udpClient.isActive)
        {
            if (Time.time - lastMessageTime >= 1 / (streamingFrequency * MAX_STREAMING_FREQUENCY))
            {
                lastMessageTime = Time.time;
                _udpClient.SendMessage(BuildString());
            }
        }
    }

    class SensorBoolPair
    {
        public Sensor sensor;
        public bool wasStreaming = false;
        public SensorBoolPair(Sensor sensor)
        {
            this.sensor = sensor;
        }
    }
}
