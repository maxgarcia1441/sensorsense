using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.Android;
using AndroidNativeCore;

public class SaveData : MonoBehaviour
{
    private Dictionary<string, string[]> data;
    public StateData stateData;
    private Sensor[] _sensors;

    void Start()
    {

        data = stateData.sensorData;
        if (!Permission.HasUserAuthorizedPermission(Permission.ExternalStorageWrite))
        {
            Permission.RequestUserPermission(Permission.ExternalStorageWrite);
        }
        _sensors = FindObjectsOfType<Sensor>();
    }

    public void Record(string dataType, string value, float timeStep)
    {
        if (data.ContainsKey(dataType))
        {
            if (data[dataType][0] == null || Time.time - float.Parse(data[dataType][0]) >= timeStep)
            {
                Debug.Log("elapsed");
                data[dataType][0] = Time.time.ToString("0.00");
                data[dataType][1] += DateTime.Now.ToString().Replace(",", ".") + ", ";
                data[dataType][1] += value + "\n";
            }
        }
        else
        {
            Debug.Log("New dataType " + dataType + " added");
            string[] stampedValue = new string[2];    //[time created, valuestring]
            stampedValue[0] = Time.time.ToString("0.00");
            stampedValue[1] = DateTime.Now.ToString().Replace(",", ".") + ", ";
            stampedValue[1] += value + "\n";
            data.Add(dataType, stampedValue);
        }
    }

    public void TrySaveToCSV(Sensor sensor)
    {
        string dataType = sensor.GetType().ToString();
        if (data.ContainsKey(dataType))
        {
            SaveCSV.SaveCSVToFile(data[dataType][1], DataTitle(sensor) + dataType);
        }
        else
        {
            Debug.LogError(dataType + "Unable to save file, data type unknown");
        }
    }

    public void TryShareCSV(Sensor sensor)
    {
        string dataType = sensor.GetType().ToString();
        if (data.ContainsKey(dataType))
        {
            AndroidCore.share("Saving File", DataTitle(sensor) + data[dataType][1]);
        }
        else
        {
            Debug.LogError(dataType + "Sharing Failed, data type unknown");
        }
    }

    private static string DataTitle(Sensor sensor)
    {
        string title = "Time, ";
        for (int i = 0; i < sensor.ValueNames.GetLength(0); i++)
        {
            title += sensor.ValueNames[i, 0];
            title += ", ";
        }
        return title.Substring(0, title.Length - 2) + "\n";
    }

    void Update()
    {
        for (int i = 0; i < _sensors.Length; i++)
        {
            if (_sensors[i].isRecording && _sensors[i].IsAvailable)
            {
                Record(_sensors[i].GetType().ToString(), _sensors[i].ValueString, 1.0f / _sensors[i].samplingRate);
            }
        }
    }

    public void SetRecording(string testObject, bool state)
    {
        bool found = false;
        for (int i = 0; i < _sensors.Length; i++)
        {
            if (Type.GetType(testObject) == _sensors[i].GetType())
            {
                _sensors[i].isRecording = state;
                found = true;
            }
        }
        if (!found)
        {
            Debug.LogError(testObject + " is not a class");
        }
    }

    public bool ContainsData(Type sensorType)
    {
        return data.ContainsKey(sensorType.ToString());
    }
}
