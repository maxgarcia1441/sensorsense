using UnityEngine;
using System.IO;
using UnityEngine.Android;

public class SaveCSV
{
    public static void SaveCSVToFile(string csvData, string fileName)
    {
        fileName += ".csv";
        if (!Application.isEditor)
        {
            string folderPath = GetDownloadFolderPath();
            string filePath = Path.Combine(folderPath, fileName);
            byte[] bytes = System.Text.Encoding.UTF8.GetBytes(csvData);

            File.WriteAllBytes(filePath, bytes);
            Debug.Log("CSV file saved to: " + filePath);
        }
        else
        {
            Debug.LogAssertion("Editor Mode");
            Debug.Log(csvData);
        }
    }

    private static string GetDownloadFolderPath()
    {
        AndroidJavaClass environment = new AndroidJavaClass("android.os.Environment");
        AndroidJavaObject directoryDownloads = environment.CallStatic<AndroidJavaObject>("getExternalStoragePublicDirectory", environment.GetStatic<string>("DIRECTORY_DOWNLOADS"));
        string downloadFolderPath = directoryDownloads.Call<string>("getAbsolutePath");
        return downloadFolderPath;
    }
}