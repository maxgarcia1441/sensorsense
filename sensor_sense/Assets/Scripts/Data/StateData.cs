using UnityEngine;
using System.Collections;
using System.Collections.Generic;
[CreateAssetMenu()]
public class StateData : ScriptableObject {
    public bool UDPServerDisplayData = false;
    public bool gyroControl = true;
    public Quaternion offsetRotation;
    public StreamingScreen clientServerPair;
    public string[] localIPs;
    public Dictionary<string, string[]> sensorData = new Dictionary<string, string[]>();
    public bool gyroActivated;
    public bool lightActivated;
    public bool locationActivated;
    public bool microphoneActivated;
    public bool accelerometerActivated;
}