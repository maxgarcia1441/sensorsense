## Exercise 1: Basic Sensor Data Collection and Visualization

**Objective:** Introduce students to the app and basic sensor data collection and visualization techniques.

1. **Task:** Record accelerometer and gyroscope data using the app while performing different motions (e.g., shaking, rotating).
   **LabVIEW/Python Task:** Plot the recorded data using simple line plots in LabVIEW or Python.

2. **Task:** Collect light sensor data by exposing the phone to various lighting conditions.
   **LabVIEW/Python Task:** Create a basic graph to display the light sensor values over time.

[Jupyter Notebook](../exercises/Python/ex1_plotting_data.ipynb), [Python code](../exercises/Python/ex1_plotting_data_SOLUTION.py), [LabVIEW](../exercises/LabVIEW/ex1_plotting_data_SOLUTION.vi)
![LabVIEW Solution](../exercises/images/ex_1.png)

## Exercise 2: Sensor Fusion and Calibration
**Objective:** Teach students about sensor fusion and the importance of calibration.

1. **Task:** Use the app to record accelerometer and gyroscope data together. Calculate the device's orientation using basic sensor fusion techniques.
   **LabVIEW/Python Task:** Develop a simple LabVIEW or Python script to calculate and visualize orientation from the combined sensor data.

2. **Task:** Explore the app's calibration features and observe its effect on orientation accuracy.
   **LabVIEW/Python Task:** Create before-and-after plots of orientation data to showcase the impact of calibration.

[Jupyter Notebook](../exercises/Python/ex2_calibrating.ipynb), [Python code](../exercises/Python/ex2_calibrating_SOLUTION.py), [LabVIEW](../exercises/LabVIEW/ex2_calibrating_SOLUTION.vi)
![LabVIEW Solution](../exercises/images/ex_2.jpeg)

## Exercise 3: Real-time Data Streaming and Analysis
**Objective:** Introduce students to real-time data streaming and analysis using the UDP protocol.

1. **Task:** Stream accelerometer data in real-time using the app's UDP protocol feature.
   **LabVIEW/Python Task:** Develop a basic LabVIEW or Python program to receive and visualize real-time accelerometer data.

2. **Task:** Demonstrate real-time motion detection using streamed accelerometer data.
   **LabVIEW/Python Task:** Implement a basic algorithm in LabVIEW or Python to detect specific motions based on real-time accelerometer readings.

[Jupyter Notebook](../exercises/Python/ex3_real_time_plotting.ipynb), [Python code](../exercises/Python/ex3_real_time_plotting_SOLUTION.py), [LabVIEW](../exercises/LabVIEW/ex3_real_time_plotting_SOLUTION.vi)