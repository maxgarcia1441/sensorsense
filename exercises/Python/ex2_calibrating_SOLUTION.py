import pandas as pd
import matplotlib.pyplot as plt
from scipy.spatial.transform import Rotation as R
import numpy as np

def calculate_angular_velocities(rotation_data, dt):
    rotation_angles = np.array(rotation_data)

    # Estimate angular velocities using finite differences and provided dt
    angular_velocities = np.diff(rotation_angles, axis=0) / dt
    initial_rotation = np.array([[0, 0, 0]])
    return np.concatenate((initial_rotation, angular_velocities), axis=0)

def calculate_average_dt(timestamps):
    unique_timestamps, unique_indices = np.unique(timestamps, return_index=True)
    unique_timestamps = np.array([np.datetime64(ts) for ts in unique_timestamps])

    time_diffs = np.diff(unique_timestamps) / np.timedelta64(1, 's')

    num_samples = len(timestamps) - 1
    total_time_diff = np.sum(time_diffs)
    
    average_dt = total_time_diff / num_samples

    return average_dt

# Complementary filter function
def complementary_filter(gyro_data, accel_data, dt, alpha):
    roll, pitch, yaw = 0.0, 0.0, 0.0
    orientation_angles = []

    for i in range(len(gyro_data)):
        # Extract gyroscope data (angular velocity)
        gyro_roll, gyro_pitch, gyro_yaw = gyro_data[i]
        
        # Integrate gyro data to get incremental angles
        gyro_roll_delta = gyro_roll * dt
        gyro_pitch_delta = gyro_pitch * dt
        gyro_yaw_delta = gyro_yaw * dt
        
        # Update roll, pitch, and yaw using gyroscope data
        roll += gyro_roll_delta
        pitch += gyro_pitch_delta
        yaw += gyro_yaw_delta
        
        # Normalize accelerometer data
        accel_norm = np.linalg.norm(accel_data[i])
        
        # Calculate roll and pitch angles from accelerometer data
        accel_roll = np.arctan2(accel_data[i][1], accel_data[i][2])
        accel_pitch = np.arctan2(-accel_data[i][0], accel_norm)
        
        # Apply complementary filter
        roll = alpha * roll + (1 - alpha) * accel_roll
        pitch = alpha * pitch + (1 - alpha) * accel_pitch
        
        # Append the orientation angles to the list
        orientation_angles.append([roll, pitch, yaw])
    
    return np.array(orientation_angles)


# Read gyroscope data from gyroscope2.csv
gyro_data = pd.read_csv('gyroscope2.csv', header=None, names=['Timestamp', 'X', 'Y', 'Z'])
gyro_data['Timestamp'] = pd.to_datetime(gyro_data['Timestamp'])

# Read accelerometer data from accelerometer2.csv
accel_data = pd.read_csv('accelerometer2.csv', header=None, names=['Timestamp', 'X', 'Y', 'Z'])
accel_data['Timestamp'] = pd.to_datetime(accel_data['Timestamp'])

# Convert Euler angles to radians
gyro_data[['X', 'Y', 'Z']] = gyro_data[['X', 'Y', 'Z']].apply(np.radians)

# Create a 3D array
orientation_array = gyro_data[['X', 'Y', 'Z']].values
accel_array = accel_data[['X', 'Y', 'Z']].values

# Define filter parameters
alpha = 0.98  # Complementary filter factor
dt = calculate_average_dt(accel_data['Timestamp'])

angular_velocities = calculate_angular_velocities(gyro_data[['X', 'Y', 'Z']].values, dt)

# Apply complementary filter to obtain orientation angles
calibrated_rotation = complementary_filter(angular_velocities, accel_data[['X', 'Y', 'Z']].values, dt, alpha)

# Add the initial rotation matrix to all data
calibrated_rotation[:, 0] += gyro_data['X'][0]
calibrated_rotation[:, 1] += gyro_data['Y'][0]
calibrated_rotation[:, 2] += gyro_data['Z'][0]

# Normalize it to be within 0 and 360 degrees
calibrated_rotation = calibrated_rotation % (2 * np.pi)

rotation_degrees = np.degrees(calibrated_rotation)

# Create a figure with two subplots
fig, axs = plt.subplots(1, 2, figsize=(12, 6))

# Plot gyroscope data
axs[0].plot(gyro_data['Timestamp'], np.degrees(gyro_data['X']), label='Gyro X', color='red', alpha=0.7)
axs[0].plot(gyro_data['Timestamp'], np.degrees(gyro_data['Y']), label='Gyro Y', color='green', alpha=0.5)
axs[0].plot(gyro_data['Timestamp'], np.degrees(gyro_data['Z']), label='Gyro Z', color='blue', alpha=0.3)
axs[0].set_title('Raw gyroscope Data')
axs[0].set_xlabel('Timestamp')
axs[0].set_ylabel('Value')
axs[0].legend()
axs[0].grid()

# Plot the roll, pitch, and yaw angles after calibration
axs[1].plot(gyro_data['Timestamp'], rotation_degrees[:,0], label='Roll', color='red', alpha=0.7)
axs[1].plot(gyro_data['Timestamp'], rotation_degrees[:,1], label='Pitch', color='green', alpha=0.5)
axs[1].plot(gyro_data['Timestamp'], rotation_degrees[:,2], label='Yaw', color='blue', alpha=0.3)
axs[1].set_title('Calibrated orientation')
axs[1].set_xlabel('Timestamp')
axs[1].set_ylabel('Angle (degrees)')
axs[1].grid()
axs[1].legend()

plt.show()