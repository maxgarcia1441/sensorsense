# Python Installation and Library Setup Tutorial

In this tutorial, we will walk you through the process of installing Python, along with some essential libraries like Pandas and Matplotlib, and setting up Jupyter Notebook for a convenient coding and visualization environment.

## Table of Contents

1. [Python Installation](#python-installation)
2. [Library Installation with Pip](#library-installation-with-pip)
3. [Installing and Running Jupyter Notebook](#installing-and-running-jupyter-notebook)

## 1. Python Installation

First, let's ensure that Python is installed on your system. We recommend using Python 3.6 or a higher version. To check if Python is already installed, open your terminal and run:

```
python3 --version
```

If Python is not installed, you can download it from the [official Python website](https://www.python.org/downloads/).

## Library Installation with Pip

Once you have Python installed, you can use the pip package manager to install libraries like Pandas and Matplotlib. Open your terminal and run the following commands:

```
pip3 install pandas
pip3 install matplotlib
```

These commands will download and install the Pandas and Matplotlib libraries, which are essential for data analysis and visualization in Python.

## Installing and Running Jupyter Notebook

### Installing Jupyter Notebook

Jupyter Notebook is a powerful tool that allows you to create and share documents containing live code, equations, visualizations, and explanatory text. To install Jupyter Notebook, use the following command:

```
pip3 install jupyter
```

### Running Jupyter Notebook

After installing Jupyter Notebook, you can run it from the terminal by navigating to the directory where you want to create your notebook and running:

```
jupyter notebook
```

This command will launch the Jupyter Notebook server, and a new tab will open in your default web browser with the Jupyter interface.

### Using Visual Studio Code (Optional)

If you prefer to use Visual Studio Code for your coding tasks, you can integrate Jupyter Notebook capabilities directly into the VS Code environment. Here's how to set it up:

1. **Install Visual Studio Code:** If you don't have Visual Studio Code installed, download and install it from the [official website](https://code.visualstudio.com/).

2. **Install Python Extension:** Open Visual Studio Code, go to the Extensions view by clicking the square icon in the sidebar on the left, or use the keyboard shortcut `Ctrl + Shift + X`. Search for "Python" and install the official Python extension provided by Microsoft.

3. **Select Python Interpreter:** After installing the Python extension, open a Python (.py) file or a Jupyter Notebook (.ipynb) file. Click on the Python version in the bottom status bar, or use the keyboard shortcut `Ctrl + Shift + P` to open the command palette and search for "Python: Select Interpreter". Choose the Python 3.6 interpreter from the list.

By following these steps, you can seamlessly use Visual Studio Code to work with Python 3.6 and Jupyter Notebook files, benefiting from the rich features and capabilities of both tools.
