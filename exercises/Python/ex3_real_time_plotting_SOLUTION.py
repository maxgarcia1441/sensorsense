import socket
import matplotlib.pyplot as plt
import numpy as np
from matplotlib.animation import FuncAnimation

# UDP socket setup
UDP_IP = "0.0.0.0"  # Listen on all available interfaces
UDP_PORT = 8888
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
sock.bind((UDP_IP, UDP_PORT))
# sock.settimeout(0.5)

# Create a figure and axis
fig, ax = plt.subplots()
xdata, ydata, zdata = [], [], []
ln_x, = plt.plot([], [], 'r', label='X', animated=True)
ln_y, = plt.plot([], [], 'g', label='Y', animated=True)
ln_z, = plt.plot([], [], 'b', label='Z', animated=True)
ax.legend()

def init():
    #ax.set_xlim(0, 120)  # Set appropriate x-axis limits
    ax.set_ylim(0, 360)  # Set appropriate y-axis limits for angles
    return ln_x, ln_y, ln_z

def update(frame):
    data, _ = sock.recvfrom(1024)
    data_str = data.decode("utf-8")
    timestamp_str, x_str, y_str, z_str = data_str.strip().split(", ")
    x_value = float(x_str)
    y_value = float(y_str)
    z_value = float(z_str)
    #timestamp = datetime.strptime(timestamp_str, "%d/%m/%Y %I:%M:%S %p")

    xdata.append(x_value)
    ydata.append(y_value)
    zdata.append(z_value)

    ln_x.set_data(range(len(xdata)), xdata)
    ln_y.set_data(range(len(ydata)), ydata)
    ln_z.set_data(range(len(zdata)), zdata)
    ax.relim()
    ax.autoscale_view()

    return ln_x, ln_y, ln_z

ani = FuncAnimation(fig, update, frames=range(10),
                    init_func=init, blit=True, interval=100)

plt.show()