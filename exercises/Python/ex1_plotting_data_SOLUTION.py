import pandas as pd
import matplotlib.pyplot as plt

# Read gyroscope data from gyroscope2.csv
gyro_data = pd.read_csv('gyroscope.csv', header=None, names=['Timestamp', 'X', 'Y', 'Z'])
gyro_data['Timestamp'] = pd.to_datetime(gyro_data['Timestamp'])

# Calculate the time span of the gyroscope data
time_span = gyro_data['Timestamp'].iloc[-1] - gyro_data['Timestamp'].iloc[0]

# Print the time span (length of the X-axis)
print("Length of X-axis (Gyroscope Data):", time_span)
print("Length of array:", len(gyro_data))

# Read accelerometer data from accelerometer2.csv
accel_data = pd.read_csv('accelerometer.csv', header=None, names=['Timestamp', 'X', 'Y', 'Z'])
accel_data['Timestamp'] = pd.to_datetime(accel_data['Timestamp'])

# Create a figure with two subplots
fig, axs = plt.subplots(1, 2, figsize=(12, 6))

# Plot gyroscope data
axs[0].plot(gyro_data['Timestamp'], gyro_data['X'], label='Gyro X', color='blue', alpha=0.7)
axs[0].plot(gyro_data['Timestamp'], gyro_data['Y'], label='Gyro Y', color='blue', alpha=0.5)
axs[0].plot(gyro_data['Timestamp'], gyro_data['Z'], label='Gyro Z', color='blue', alpha=0.3)
axs[0].set_title('Gyroscope Data')
axs[0].set_xlabel('Timestamp')
axs[0].set_ylabel('Value')
axs[0].legend()
axs[0].grid()

# Plot accelerometer data
axs[1].plot(accel_data['Timestamp'], accel_data['X'], label='Accel X', color='red', alpha=0.7)
axs[1].plot(accel_data['Timestamp'], accel_data['Y'], label='Accel Y', color='red', alpha=0.5)
axs[1].plot(accel_data['Timestamp'], accel_data['Z'], label='Accel Z', color='red', alpha=0.3)
axs[1].set_title('Accelerometer Data')
axs[1].set_xlabel('Timestamp')
axs[1].set_ylabel('Value')
axs[1].legend()
axs[1].grid()

# Adjust layout
plt.tight_layout()

# Show the plots
plt.show()
