# Sensor Sense - Android App for Sensor Data Visualization

- [Download the Android Application](/sensor_sense/Build/app.apk)

- [The source code](/sensor_sense/Assets/Scripts)

## Overview

Sensor Sense is an Android app developed using Unity, designed to provide access to phone sensors and offer a more technical use with data visualization in graphs. The primary focus of this app is to cater to technical users who require detailed sensor data and graphical representations for analysis and research purposes.

## Features

- Access to Phone Sensors: Sensor Sense allows users to access a wide range of phone sensors, including accelerometers, gyroscopes, magnetometers, light sensors, and more.

- Graph Visualization: The app provides graph visualization capabilities to display the data collected from the phone sensors. Users can observe sensor data trends over time, enabling in-depth analysis.

## Purpose

The purpose of this project is to offer a powerful tool that allows technical users to access sensor data from their Android devices and utilize advanced data visualization techniques to make informed decisions and gain insights from the data.

## Solution in Progress

To address the graph positioning issue, the development team is actively working on implementing a solution that allows precise control of the graph's positioning through code. By doing so, the team aims to ensure that the graph displays correctly on the screen, providing an optimal user experience.

## Future Roadmap

The development of Sensor Sense is ongoing, with several planned enhancements and features in the pipeline:

- Expanded Sensor Support: The app will continue to add support for more phone sensors, ensuring users have access to a comprehensive range of data.

- Customizable Graphs: Users will have the ability to customize graph visuals, including colors, scales, and data ranges, according to their specific requirements.

- Export and Sharing: Sensor Sense will allow users to export sensor data and graph visualizations for further analysis or sharing with colleagues.

## License

<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a><br /><span xmlns:dct="http://purl.org/dc/terms/" href="http://purl.org/dc/dcmitype/InteractiveResource" property="dct:title" rel="dct:type">SensorSense</span> by <a xmlns:cc="http://creativecommons.org/ns#" href="https://gitlab.com/maxgarcia1441/sensorsense" property="cc:attributionName" rel="cc:attributionURL">University of Oulu</a> is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>.